<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Admin\Model;
use Think\Model;
/**
 * 菜单模型
 * @author jry <598821125@qq.com>
 */
class MenuModel extends Model{
    /**
     * 自动验证规则
     * @author jry <598821125@qq.com>
     */
    protected $_validate = array(
        array('title','require','菜单标题必须填写', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('url','require','链接地址必须填写', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /**
     * 自动完成规则
     * @author jry <598821125@qq.com>
     */
    protected $_auto = array(
        array('ctime', NOW_TIME, self::MODEL_INSERT),
        array('utime', NOW_TIME, self::MODEL_BOTH),
        array('sort', '0', self::MODEL_INSERT),
        array('status', '1', self::MODEL_INSERT),
    );

    /**
     * 根据ID获取菜单
     * @author jry <598821125@qq.com>
     */
    public function getMenuById($id){
        $map['id'] = array('eq', $id);
        return $this->where($map)->find();
    }

    /**
     * 获取所有菜单
     * @author jry <598821125@qq.com>
     */
    public function getAllMenu($map, $status = '0,1'){
        $map['status'] = array('in', $status);
        return $this->where($map)->order('id asc')->select();
    }

    /**
     * 根据PID获取不同级别的菜单
     * @author jry <598821125@qq.com>
     */
    public function getMenuByPid($pid = 0){
        $map['pid'] = array('eq', $pid);
        $map['status'] = array('eq', 1);
        return $this->where($map)->order('sort asc')->select();
    }

    /**
     * 根据id获取当前菜单的顶级菜单
     * @author jry <598821125@qq.com>
     */
    public function getMainMenuById($id){
        $map['id'] = array('eq', $id);
        $map['status'] = array('eq', 1);
        $main_menu = array();
        do{
            $main_menu = $this->where($map)->find();
            $map['id'] = array('eq', $main_menu['pid']);
        }while($main_menu['pid'] > 0);
        return $main_menu;
    }

    /**
     * 根据Controller和Action获取当前菜单（PID!＝0）
     * @author jry <598821125@qq.com>
     */
    public function getMenuByControllerAndAction(){
        $map['status'] = array('eq', 1);
        $map['pid']    = array('neq', 0);
        $map['url'] = array('like', CONTROLLER_NAME.'/'.ACTION_NAME.'%');
        $result = $this->where($map)->find();
        if(!$result){
            $map['url'] = array('like', CONTROLLER_NAME.'/index%');
            $result = $this->where($map)->find();
        }
        return $result;
    }

    /**
     * 根据主菜单ID获取子菜单
     * @author jry <598821125@qq.com>
     */
    public function getSideMenuByMainMenuId($id){
        $map['status'] = array('eq', 1);
        $map['pid']    = array('eq', $id);
        $child_menu = $this->where($map)->order('sort asc')->select();
        foreach($child_menu as $key => $val){
            $con['status'] = array('eq', 1); 
            $con['pid']    = array('eq', $val['id']);
            $child_menu[$key]['_child'] = $this->where($con)->order('sort asc')->select();
        }
        return $child_menu;
    }
}
