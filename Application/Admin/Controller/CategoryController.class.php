<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
/**
 * 后台分类控制器
 * @author jry <598821125@qq.com>
 */
class CategoryController extends AdminController{
    /**
     * 分类列表
     * @author jry <598821125@qq.com>
     */
    public function index($pid = null){
        $all_category = D('Common/Tree')->toFormatTree(D('Category')->getAllCategory());
        $this->assign('volist', $this->int_to_icon($all_category));
        $this->assign('meta_title', "分类列表");
        $this->display();
    }

    /**
     * 新增分类
     * @author jry <598821125@qq.com>
     */
    public function add(){
        if(IS_POST){
            $Category = D('Category');
            $data = $Category->create();
            if($data){
                $id = $Category->add();
                if($id){
                    $this->success('新增成功', U('index'));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($Category->getError());
            }
        }else{
            $all_category = D('Common/Tree')->toFormatTree(D('Category')->getAllCategory($map));
            $all_category = array_merge(array(0 => array('id'=>0, 'title_show'=>'顶级分类')), $all_category);
            $this->assign('all_category', $all_category);
            $this->assign('all_model', D('Model')->getAllModel());
            $this->meta_title = '新增分类';
            $this->display('edit');
        }
    }

    /**
     * 编辑分类
     * @author jry <598821125@qq.com>
     */
    public function edit($id = 0){
        if(IS_POST){
            $Category = D('Category');
            $data = $Category->create();
            if($data){
                if($Category->save()!== false){
                    $this->success('更新成功', U('index'));
                }else{
                    $this->error('更新失败');
                }
            }else{
                $this->error($Category->getError());
            }
        }else{
            $info = D('Category')->getCategoryById($id);
            $all_category = D('Common/Tree')->toFormatTree(D('Category')->getAllCategory());
            $all_category = array_merge(array(0 => array('id'=>0, 'title_show'=>'顶级分类')), $all_category);
            $this->assign('all_category', $all_category);
            $this->assign('all_model', D('Model')->getAllModel());
            $this->assign('info', $info);
            $this->meta_title = '编辑分类';
            $this->display();
        }
    }
}
