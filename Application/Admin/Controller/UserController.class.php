<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Think\Controller;
/**
 * 后台用户控制器
 * @author jry <598821125@qq.com>
 */
class UserController extends AdminController{
    /**
     * 用户列表
     * @author jry <598821125@qq.com>
     */
    public function index(){
        $all_user = D('User')->page(!empty($_GET["p"])?$_GET["p"]:1, C('ADMIN_PAGE_ROWS'))->getAllUser();
        $page = new \Think\Page(D('User')->count(), C('ADMIN_PAGE_ROWS'));
        $this->assign('page', $page->show());
        $this->assign('volist', $this->int_to_icon($all_user));
        $this->assign('meta_title', "用户列表");
        $this->display();
    }

    /**
     * 新增用户
     * @author jry <598821125@qq.com>
     */
    public function add(){
        if(IS_POST){
            $user = D('User');
            $data = $user->create();
            if($data){
                $id = $user->add();
                if($id){
                    $this->success('新增成功', U('index'));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($user->getError());
            }
        }else{
            $all_group = D('Common/Tree')->toFormatTree(D('Group')->getAllGroup());
            $all_group = array_merge(array(0 => array('id'=>0, 'title_show'=>'游荡中')), $all_group);
            $this->assign('all_group', $all_group);
            $this->meta_title = '新增用户';
            $this->display('edit');
        }
    }

    /**
     * 编辑后台用户
     * @author jry <598821125@qq.com>
     */
    public function edit($id){
        if(IS_POST){
            $user = D('User');
            //不修改密码时销毁变量
            if($_POST['password'] == ''){
                unset($_POST['password']);
            }else{
                $_POST['password'] = user_md5($_POST['password']);
            }
            //不允许更改超级管理员用户组
            if($_POST['id'] == 1){
                unset($_POST['group']);
            }
            if($_POST['extend']){
                $_POST['extend'] = json_encode($_POST['extend']);
            }
            if($user->save($_POST)){
                $user->updateUserCache($_POST['id']);
                $this->success('更新成功', U('index'));
            }else{
                $this->error('更新失败', $user->getError());
            }
        }else{
            $all_group = D('Common/Tree')->toFormatTree(D('Group')->getAllGroup());
            $all_group = array_merge(array(0 => array('id'=>0, 'title_show'=>'游荡中')), $all_group);
            $this->assign('all_group', $all_group);
            $this->assign('info', D('User')->getUserById($id));
            $this->meta_title = '编辑用户';
            $this->display();
        }
    }

    /**
     * 设置一条或者多条数据的状态
     * 重载AdminController中的公共setStatus方法以便于阻止对UID为1的超级管理员账户修改
     * @author jry <598821125@qq.com>
     */
    public function setStatus($model = CONTROLLER_NAME){
        $ids    = I('request.ids');
        $status = I('request.status');
        if(empty($ids)){
            $this->error('请选择要操作的数据');
        }
        if(in_array(1, $ids, true) || 1 == $ids){
            $this->error('不允许更改超级管理员状态');
        }
        $map['id'] = array('in',$ids);
        switch($status){
            case 'delete'  : //假删除条目
                $data['status'] = -1;
                $this->editRow($model, $data, $map, array('success'=>'删除成功','error'=>'删除失败'));
                break;
            case 'forbid'  : //禁用条目
                $data = array('status' => 0);
                $this->editRow($model, $data, $map, array('success'=>'禁用成功','error'=>'禁用失败'));
                break;
            case 'resume'  : //恢复条目
                $data = array('status' => 1);
                $this->editRow($model, $data, $map, array('success'=>'恢复成功','error'=>'恢复失败'));
                break;
            case 'restore' : //从假删除状态还原条目
                $data = array('status' => 1);
                $map  = array_merge(array('status' => -1), $map);
                $this->editRow($model, $data, $map, array('success'=>'启用成功','error'=>'启用失败'));
                break;
            default :
                $this->error('参数错误');
                break;
        }
    }
}
