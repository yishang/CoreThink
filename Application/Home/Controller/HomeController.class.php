<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
/**
 * 前台公共控制器
 * 为防止多分组Controller名称冲突，公共Controller名称统一使用分组名称
 * @author jry <598821125@qq.com>
 */
class HomeController extends Controller{
    /**
     * 初始化方法
     * @author jry <598821125@qq.com>
     */
    protected function _initialize(){
        //读取数据库中的配置
        $config = S('DB_CONFIG_DATA');
        if(!$config){
            $config = D('Admin/Config')->lists();
            S('DB_CONFIG_DATA',$config);
        }
        //模板相关配置
        $config['TMPL_PARSE_STRING']['__PUBLIC__'] = __ROOT__.'/Public';
        $config['TMPL_PARSE_STRING']['__IMG__'] = __ROOT__.'/Application/'.MODULE_NAME.'/View/'.$config['DEFAULT_THEME'].'/Public/img';
        $config['TMPL_PARSE_STRING']['__CSS__'] = __ROOT__.'/Application/'.MODULE_NAME.'/View/'.$config['DEFAULT_THEME'].'/Public/css';
        $config['TMPL_PARSE_STRING']['__JS__']  = __ROOT__.'/Application/'.MODULE_NAME.'/View/'.$config['DEFAULT_THEME'].'/Public/js';
        C($config); //添加配置

        if(!C('TOGGLE_WEB_SITE')){
            $this->error('站点已经关闭，请稍后访问~');
        }

        $this->assign('__USER__', session('user_auth')); //用户登录信息
    }

    /**
     * select返回的数组进行整数映射转换
     * @param array $map  映射关系二维数组  array('字段名1'=>array(映射关系数组), '字段名2'=>array(映射关系数组), ...)
     * @return array  array(array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常', ...))
     * @author jry <598821125@qq.com>
     */
    function int_to_icon(&$data, $map = array('status' => array(
                            1  => '<i class="icon-ok" style="color:green"></i>',
                            -1 => '<i class="icon-trash" style="color:red"></i>',
                            0  => '<i class="icon-ban-circle" style="color:red"></i>'))) {
        if($data === false || $data === null ){
            return $data;
        }
        $data = (array)$data;
        foreach ($data as $key => $row){
            foreach ($map as $col=>$pair){
                if(isset($row[$col]) && isset($pair[$row[$col]])){
                    $data[$key][$col.'_text'] = $pair[$row[$col]];
                }
            }
        }
        return $data;
    }
}
