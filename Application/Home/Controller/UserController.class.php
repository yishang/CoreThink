<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
/**
 * 用户控制器
 * @author jry <598821125@qq.com>
 */
class UserController extends HomeController{
    /**
     * 默认方法
     * @author jry <598821125@qq.com>
     */
    public function index(){
        $this->assign('meta_title', "首页");
        $this->display();
    }

    /**
     * 登陆
     * @author jry <598821125@qq.com>
     */
    public function login(){
        if(IS_POST){
            $username = I('username');
            $password = I('password');
            if(!$username){
                $this->error('请输入账号！');
            }
            if(!$password){
                $this->error('请输入密码！');
            }
            $uid = D('Admin/User')->login($username, $password);
            if(0 < $uid){
                $this->success('登录成功！', U('Index/index'));
            }else{
                $this->error('登录失败，'. $uid);
            }
        }else{
            if(is_login()){
                $this->error("您已登陆系统", U('Index/index'));
            }
            $this->meta_title = '用户登录';
            $this->display();
        }
    }

    /**
     * 注销
     * @author jry <598821125@qq.com>
     */
    public function logout(){
        session('user_auth', null);
        session('user_auth_sign', null);
        $this->success('退出成功！', U('User/login'));
    }

    /**
     * 用户注册
     * @author jry <598821125@qq.com>
     */
    public function register(){
        if(IS_POST){
            if(!C('TOGGLE_USER_REGISTER')){
                $this->error('注册已关闭！');
            }
            $reg_type = $_POST['reg_type'];
            switch($reg_type){
                case 'mobile':
                    $username = $_POST['mobile'];
                    $_POST['username'] = 'Mobile_'.NOW_TIME;
                    break;
                case 'email':
                    $username = $_POST['email'];
                    $_POST['username'] = 'Email_'.NOW_TIME;
                    break;
            }
            //验证码严格加盐加密验证
            if(user_md5($_POST['verify'], $username) !== session('reg_verify')){
                $this->error('验证码错误！');
            }
            $password = $_POST['password'];
            $user = D('Admin/User');
            $data = $user->create();
            if($data){
                $id = $user->add();
                if($id){
                    session('reg_verify', null);
                    $uid = D('Admin/User')->login($username, $password);
                    $this->success('注册成功', U('register2'));
                }else{
                    $this->error('注册失败');
                }
            }else{
                $this->error($user->getError());
            }
        }else{
            if(is_login()){
                $this->error("您已登陆系统", U('Index/index'));
            }
            $this->meta_title = '用户注册';
            $this->display();
        }
    }

    /**
     * 修改用户信息
     * @author jry <598821125@qq.com>
     */
    public function register2(){
        if(IS_POST){
            $user = D('Admin/User');
            $_POST['id'] = is_login();
            $result = $user->updateUserInfo($_POST);
            if($result){
                $user_info = $user->getUserById($_POST['id']);
                $user->autoLogin($user_info);
                $this->success('更新成功', U('Index/index'));
            }else{
                $this->error($user->getError());
            }
        }else{
            if(!is_login()){
                $this->error("请先登录系统", U('User/login'));
            }
            $this->meta_title = '完善信息';
            $this->display();
        }
    }

    /**
     * 图片验证码生成，用于登录和注册
     * @author jry <598821125@qq.com>
     */
    public function verify($vid = 1){
        $verify = new \Think\Verify();
        $verify->entry($vid);
    }

    /**
     * 邮箱验证码，用于注册
     * @author jry <598821125@qq.com>
     */
    public function sendMailVerify(){
        $receiver = I('post.email');
        if(!preg_match("/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/", $receiver)){
            $this->error('邮箱格式不正确！');
        }
        $reg_verify = randString(); //生成验证码
        session('reg_verify', user_md5($reg_verify, $receiver));
        $body = '注册验证码：'.$reg_verify;
        if(send_mail($receiver, '注册验证', $body)){
            $this->success('发送成功，请登陆邮箱查收！');
        }else{
            $this->error('发送失败！');
        }
    }

    /**
     * 短信验证码，用于注册
     * @author jry <598821125@qq.com>
     */
    public function sendMobileVerify(){
        $receiver = I('post.mobile');
        if(!preg_match("/^1\d{10}$/", $receiver)){
            $this->error('手机号格式不正确！');
        }
        $reg_verify = randString(); //生成验证码
        session('reg_verify', user_md5($reg_verify, $receiver));
        $body = '注册验证码：'.$reg_verify;
        if(send_message($receiver, '注册验证', $body)){
            $this->success('发送成功，请查收！');
        }else{
            $this->error('发送失败！');
        }
    }
}
